//5 How to convert Fahrenheit to Celsius Program in java 
 
import java.util.Scanner;
 
class Question05 {

  public static void main(String[] args){

    System.out.println(" Fahrenheit to Celsius: ");

    System.out.println("200 F = " + convertFahrenheitToCelsius(200)+" C" );
    System.out.println("150 F = " + convertFahrenheitToCelsius(150)+" C" );
    System.out.println("100 F = " + convertFahrenheitToCelsius(100)+" C" );
    System.out.println(" 50 F = " + convertFahrenheitToCelsius(50)+" C" );    

  }

  private static double convertFahrenheitToCelsius(double f) {
    return (f-32)/1.8;
  }
 
}
