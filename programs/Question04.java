//4 How to get Using input using Scanner Program in java  
 
import java.util.Scanner;
 
class Question04 {

  public static void main(String[] args){

    Scanner scanner = new Scanner(System.in);

    System.out.print("What is your name: ");

    String name = scanner.next();

    System.out.println("Welcome " + name + "!");

  }

}
