//3 Command Line Argument 

// Java 11+  try: java Question03.java 5 test % 

class Question03 {

  public static void main(String[] args){
    
    System.out.println("Arguments:");

    int counter=1;
    for (String argument : args) {

      System.out.println(counter + " -> "+ argument);

      counter++;
    }
  }

}
